import {
  Component,
  EventEmitter,
  OnChanges,
  OnInit,
  Output,
  Input,
  ChangeDetectionStrategy,
  SimpleChanges
} from '@angular/core';
import { RatingClickEvent } from '../rating-button/rating-button.component';
import { RatingValueViewModel } from '../models/rating.interface';
import { ContentRatingSummary } from '../models/content-rating-summary.interface';
import { ContentUserRating } from '../models/content-user-rating.interface';

@Component({
  selector: 'ra-star-rating-panel',
  templateUrl: './star-rating-panel.component.html',
  styleUrls: ['./star-rating-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StarRatingPanelComponent implements OnChanges, OnInit {

  @Input() isProcessing = false;
  @Input() contentItem: ContentRatingSummary;
  @Input() processingDisplayString = 'Loading...';
  @Input() maxStarRating;
  @Input() title: string;
  @Input() dismissBtnTitle = 'Cancel';
  @Input() affirmativeBtnTitle = 'Done';

  @Output() ratingDismissClicked: EventEmitter<void> = new EventEmitter<void>();
  @Output() ratingFinalised: EventEmitter<ContentUserRating> = new EventEmitter<ContentUserRating>();

  selectedRating = 0;
  ratings: RatingValueViewModel[];

  constructor() { }

  ngOnInit() {
    this.ratings = this.initRatingsArray(this.maxStarRating);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['maxStarRating'] && changes['maxStarRating']['currentValue']) {
      const max = changes['maxStarRating']['currentValue'];
      this.ratings = this.initRatingsArray(max);
    }
  }

  initRatingsArray(maxRating: number): RatingValueViewModel[] {
    return Array.from({length: maxRating}, (v, k) => {
      const rating = {
        rating: k + 1,
        highlighted: false,
        half: false
      };
      return rating;
    });
  }

  handleRatingClicked(event: RatingClickEvent): void {
    const actualRating = (event.halfRating) ? event.value - 0.5 : event.value;
    const ratingValIdx = event.value - 1;

    if (actualRating === this.selectedRating) {
      this.selectedRating = 0;
      this.ratings = this.ratings.map((r) => this.unhighlight(r));
    } else {
      this.selectedRating = actualRating;
      this.ratings = this.ratings.map((r, idx) => {
        return this.updateHighlightedRatingPerSelection(r, idx, ratingValIdx, event.halfRating);
      });
    }
  }

  unhighlight(r: RatingValueViewModel): RatingValueViewModel {
    r.highlighted = false;
    r.half = false;
    return r;
  }

  updateHighlightedRatingPerSelection(
    r: RatingValueViewModel,
    idx: number,
    selectedIdx: number,
    halfRating: boolean
  ): RatingValueViewModel {
      r.highlighted = (idx <= selectedIdx);
      r.half = (halfRating && idx === selectedIdx);
      return r;
  }

  handleDismissButtonClick($event) {
    this.ratingDismissClicked.emit();
  }

  handleAffirmativeButtonClick($event) {
    const cp: ContentRatingSummary = Object.assign(this.contentItem, {});
    const userRating: ContentUserRating = {
      title: cp.contentTitle,
      rating: this.selectedRating,
      contentId: cp.contentId,
      userId: cp.userId
    };
    this.ratingFinalised.emit(userRating);
  }

}
