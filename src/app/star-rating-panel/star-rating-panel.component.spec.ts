import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarRatingPanelComponent } from './star-rating-panel.component';
import { RatingValueViewModel } from '../models/rating.interface';
import { NO_ERRORS_SCHEMA } from '@angular/compiler/src/core';

describe('StarRatingPanelComponent', () => {
  let component: StarRatingPanelComponent;
  let fixture: ComponentFixture<StarRatingPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarRatingPanelComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarRatingPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  test('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('unhighlight', () => {
    // const mockRating: RatingValueViewModel = {
    //   title: 'The Dark Knight',
    //   contentId: 'd17bcdbd-d731-4968-8bae-c5021f367d42',
    //   highlighted: true
    // };
    const mockRatingValue: RatingValueViewModel = {
      rating: 3.0,
      highlighted: true,
      half: true
    };

    test('should set highlighted to false', () => {
      const m = component.unhighlight(mockRatingValue);
      expect(m.highlighted).toBe(false);
    });

    test('should set half to false', () => {
      const m = component.unhighlight(mockRatingValue);
      expect(m.half).toBe(false);
    });

  });
});
