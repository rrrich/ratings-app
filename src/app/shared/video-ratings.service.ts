import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map, delay, tap } from '../../../node_modules/rxjs/operators';
import { ContentUserRating } from '../models/content-user-rating.interface';

@Injectable({
  providedIn: 'root'
})
export class VideoRatingsService {

  private _ratings: ContentUserRating[] = [];
  private _ratings$: BehaviorSubject<ContentUserRating[]>
    = new BehaviorSubject<ContentUserRating[]>([]);

  constructor(
    private http: HttpClient
  ) { }

  submitRating(payload: ContentUserRating): Observable<boolean> {
    return of(true)
      .pipe(
        delay(3000),
        tap((v) => this.updateRatings([payload]))
      );
  }

  videoRatings(): Observable<ContentUserRating[]> {
    return this._ratings$.asObservable();
  }

  updateRatings(newRatings: ContentUserRating[]): void {
    const updated = this._ratings.concat(newRatings);
    this._ratings = updated;
    this._ratings$.next(updated);
  }

  fetchVideoRatings(): void {
    const url = `assets/json/videos.json`;
    this.http.get<object>(url)
      .pipe(
        map((response: object) => response['video_ratings']),
        tap((ratings) => this.updateRatings(ratings))
      )
      .subscribe(_ => console.log('loaded'));
  }
}
