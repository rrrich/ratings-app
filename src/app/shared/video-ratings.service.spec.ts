import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule, } from '@angular/common/http';

import { VideoRatingsService } from './video-ratings.service';

describe('VideoRatingsService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        VideoRatingsService
      ]
    });
  });

  it('should be created', inject([VideoRatingsService], (service: VideoRatingsService) => {
    expect(service).toBeTruthy();
  }));
});
