import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BsModalService, ModalModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StarRatingPanelComponent } from './star-rating-panel/star-rating-panel.component';
import { RatingButtonComponent } from './rating-button/rating-button.component';
import { VideoRatingsService } from './shared/video-ratings.service';


@NgModule({
  declarations: [
    AppComponent,
    StarRatingPanelComponent,
    RatingButtonComponent
  ],
  entryComponents: [
    StarRatingPanelComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [
    VideoRatingsService,
    BsModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
