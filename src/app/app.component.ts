import { Component, OnInit } from '@angular/core';
import { ContentRatingSummary } from './models/content-rating-summary.interface';
import { VideoRatingsService } from './shared/video-ratings.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { StarRatingPanelComponent } from './star-rating-panel/star-rating-panel.component';
import { ContentUserRating } from './models/content-user-rating.interface';


@Component({
  selector: 'ra-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'iFlix demo';
  isSubmitting = false;
  submittingDisplayLabel = 'Submitting your rating...';
  bsModalRef: BsModalRef;
  videos$: Observable<object[]>;

  constructor(
    private videosService: VideoRatingsService,
    private bsModalService: BsModalService
  ) {}

  ngOnInit() {

    this.videosService.fetchVideoRatings();

    this.videos$ = this.videosService.videoRatings()
      .pipe(
        map((reviews: ContentUserRating[]) => {
          return this.mapUserReviewsToSummaries(reviews);
        })
      );
  }

  mapUserReviewsToSummaries(reviews: ContentUserRating[]) {
    const vids2: ContentRatingSummary[] = [];
    if (!reviews.length) {
      return vids2;
    }

    reviews.forEach((r) => {

      if (vids2.find((v) => v.contentId === r['contentId'])) {
        return;
      }

      const finalisedSummary: ContentRatingSummary = reviews
        .filter((v) => v['contentId'] === r['contentId'])
        .reduce((acc, curr, idx) => {
          if (idx === 0) {
            return this.mapToContentRatingSummary(curr);
          } else {
            return {
              ...acc,
              numReviews: acc.numReviews + 1,
              totalScore: acc.totalScore + curr.rating
            };
          }
        }, {} as ContentRatingSummary);

      finalisedSummary.average = finalisedSummary.totalScore / finalisedSummary.numReviews;
      vids2.push(finalisedSummary);
    });

    return vids2;
  }

  mapToContentRatingSummary(userRating: ContentUserRating): ContentRatingSummary {
    return <ContentRatingSummary>{
      contentTitle: userRating['title'],
      userId: userRating['userId'],
      rating: userRating['rating'],
      contentId: userRating['contentId'],
      numReviews: 1,
      totalScore: userRating['rating'],
      average: 0
    };
  }

  hideRatingModal(e: Event = null): void {
    this.bsModalRef.hide();
  }

  initRatingModal(e: Event = null, video: ContentRatingSummary): void {
    const initialState = {
      contentItem: video,
      isProcessing: this.isSubmitting,
      processingDisplayString: this.submittingDisplayLabel,
      maxStarRating: 5,
      title: 'What did you think?',
      dismissBtnTitle: 'Cancel',
      affirmativeBtnTitle: 'Submit',
    };
    this.bsModalRef = this.bsModalService.show(StarRatingPanelComponent, { initialState });

    this.bsModalRef.content.ratingDismissClicked
      .subscribe(this.hideRatingModal.bind(this));
    this.bsModalRef.content.ratingFinalised
      .subscribe(this.submitRating.bind(this));
  }

  submitRating(payload: ContentUserRating) {
    this.isSubmitting = true;
    this.bsModalRef.hide();
    this.videosService.submitRating(payload)
      .subscribe((_) => {
        this.isSubmitting = false;
      });
  }
}
