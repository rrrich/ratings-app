import { AfterViewInit, Component, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { fromEvent } from '../../../node_modules/rxjs';
import { takeUntil, mergeMap, map } from 'rxjs/operators';

export interface RatingClickEvent {
  value: number;
  halfRating: boolean;
}

@Component({
  selector: 'ra-rating-button',
  templateUrl: './rating-button.component.html',
  styleUrls: ['./rating-button.component.scss']
})
export class RatingButtonComponent implements AfterViewInit {

  @ViewChild('iconContainer') iconContainer: ElementRef;

  @Input() highlightHalf: boolean;
  @Input() highlight: boolean;
  @Input() value: number;

  @Output() ratingClickEvent: EventEmitter<RatingClickEvent>
    = new EventEmitter<RatingClickEvent>();

  halfRating = false;

  constructor() { }

  ngAfterViewInit() {
    const mouseMove$ = fromEvent(this.iconContainer.nativeElement, 'mousemove');
    const mouseover$ = fromEvent(this.iconContainer.nativeElement, 'mouseover');
    const mouseout$ = fromEvent(this.iconContainer.nativeElement, 'mouseout');

    mouseover$
      .pipe(
        mergeMap(_ => {
          return mouseMove$
            .pipe(
              map(this.isMousePointerOnLeftHalf),
              takeUntil(mouseout$)
            );
        })
      )
      .subscribe((cursorLeftSide: boolean) => {
        this.halfRating = cursorLeftSide;
      });

  }

  isMousePointerOnLeftHalf(me: MouseEvent): boolean {
    const halfX = Math.round(me.srcElement['offsetWidth'] / 2);
    return (me.offsetX <= halfX);
  }

  handleRatingClick(): void {
    const eventPayload = <RatingClickEvent>{
      value: this.value,
      halfRating: this.halfRating
    };
    this.ratingClickEvent.emit(eventPayload);
  }

}
