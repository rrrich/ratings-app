export interface ContentRatingSummary {
    userId: number;
    contentId: number;
    rating: number;
    average: number;
    totalScore: number;
    numReviews: number;
    contentTitle; string;
}
