export interface RatingValueViewModel {
    rating: number;
    highlighted: boolean;
    half: boolean;
}
