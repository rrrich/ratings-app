export interface ContentUserRating {
    title: string;
    userId: number;
    contentId: number;
    rating: number;
}
